FROM python:alpine3.6

WORKDIR /usr/src/tydes
COPY . .

RUN pip install -r requirements.txt

ENTRYPOINT ["python", "app.py"]
