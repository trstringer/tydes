"""Main code execution"""

import sys
from tydes import noaa
from version import VERSION

def main():
    """Main code path"""

    if len(sys.argv) < 2:
        print('You must specify the NOAA station ID')
        sys.exit(1)

    first_arg = sys.argv[1]

    if first_arg in ['--version', '-v']:
        print(f'tydes v{VERSION}')
        sys.exit(0)

    print(str(noaa.tides_by_station(station_id=first_arg)))


if __name__ == '__main__':
    main()
