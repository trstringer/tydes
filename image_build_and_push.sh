#!/bin/sh

SCRIPT_PATH=$(dirname "$(realpath "$0")")
REPO="registry.gitlab.com/trstringer"
BIN="tydes"
BIN_VERSION="$1"
IMAGE_NAME="$REPO/$BIN:$BIN_VERSION"

docker login -u gitlab-ci-token -p "${CI_BUILD_TOKEN:-$CI_JOB_TOKEN}" registry.gitlab.com
docker build -t "$IMAGE_NAME" "$SCRIPT_PATH"
docker push "$IMAGE_NAME"
