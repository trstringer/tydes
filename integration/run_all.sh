#!/bin/bash

SCRIPT_PATH=$(dirname "$(realpath "$0")")
TEST_STATION_ID=8531680

OUTPUT=$(python "$SCRIPT_PATH/../app.py" "$TEST_STATION_ID")
if [[ $? != 0 ]]; then
    echo "$(date) - Failed to retrieve data"
    exit 1
fi

TIDE_COUNT_RETURNED=$(printf "$OUTPUT\\n" | wc -l)
printf "$OUTPUT\\n"
if [[ "$TIDE_COUNT_RETURNED" -ne 4 ]]; then
    echo "$(date) - Expected 4 tides, got $TIDE_COUNT_RETURNED"
    exit 1
fi

echo "$(date) - All tests passed"
