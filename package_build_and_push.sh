#!/bin/sh

# ensure that twine is installed
pip install --user --upgrade twine

python setup.py sdist bdist_wheel
