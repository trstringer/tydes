#!/bin/bash

echo "$(date) - Starting CI pipeline"

for JOB in "lint" "unit_test" "integration_test" "build_image"; do
    if ! gitlab-runner exec docker "$JOB"; then
        echo "$(date) - Failed on job ${JOB}"
        exit 1
    fi
done

echo "$(date) - All jobs succeeded"
