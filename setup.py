"""Setup script"""

import setuptools
from version import VERSION

with open("README.md") as readme:
    # pylint: disable=invalid-name
    long_description = readme.read()

setuptools.setup(
    name='tydes',
    version=VERSION,
    author='Thomas Stringer',
    author_email='gitlab@trstringer.com',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/trstringer/tydes',
    packages=setuptools.find_packages(),
    classifiers=(
        'Development Status :: 4 - Beta',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3'
    )
)
