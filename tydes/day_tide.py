"""DayTide class definition"""

from typing import List
from .tide_boundary import TideBoundary
from .tide_boundary_type import TideBoundaryType
from .tide_direction import TideDirection
from .tide_point import TidePoint

# pylint: disable=too-few-public-methods
class DayTide:
    """Contains the definition for the tide range of the day"""

    def __init__(self, boundaries: List[TideBoundary]):
        """Initialize the tide boundaries"""

        self.boundaries = boundaries

    def __str__(self):
        """String representation of the object"""

        output = ''
        for boundary in self.boundaries:
            if output:
                output += '\n'
            tide_boundary_type = ''
            if boundary.boundary_type == TideBoundaryType.HIGH:
                tide_boundary_type = 'HIGH'
            else:
                tide_boundary_type = 'LOW'
            output += f'{tide_boundary_type}\t{boundary.point.level} @ {boundary.point.occur_on}'

        return output

def parse_boundaries(points: List[TidePoint]) -> DayTide:
    """Take a list of tide points and parse out the boundaries"""

    previous_point: TidePoint = None
    tide_direction: TideDirection = None
    boundaries: List[TideBoundary] = []

    for tide_point in points:
        # the first tide point has nothing to compare it to, so cache it
        # and then move on
        if not previous_point:
            previous_point = tide_point
            continue
        # for the second tide point, the tide direction needs to be determined
        elif not tide_direction:
            if tide_point.level > previous_point.level:
                tide_direction = TideDirection.RISING
            else:
                tide_direction = TideDirection.FALLING
            previous_point = tide_point
            continue

        # if there are already two tide points then we have determined the tide
        # direction and we can determine a boundary

        # the tide is rising, but if was falling before then we have a boundary
        if tide_point.level > previous_point.level and tide_direction == TideDirection.FALLING:
            boundaries.append(TideBoundary(
                point=previous_point,
                boundary_type=TideBoundaryType.LOW
            ))
            tide_direction = TideDirection.RISING
        elif tide_point.level < previous_point.level and tide_direction == TideDirection.RISING:
            boundaries.append(TideBoundary(
                point=previous_point,
                boundary_type=TideBoundaryType.HIGH
            ))
            tide_direction = TideDirection.FALLING

        previous_point = tide_point

    return DayTide(boundaries[:4])
