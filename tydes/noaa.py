"""Proxy class to interact with the NOAA API"""

from datetime import datetime, timedelta
import requests
from .day_tide import parse_boundaries
from .tide_point import parse_tide_points

def _make_noaa_request(station_id: int, date: datetime) -> str:
    """Make the web API request to the NOAA API"""

    current_day_formatted = date.strftime('%Y%m%d')
    tomorrow_formatted = (date + timedelta(days=1)).strftime('%Y%m%d')

    # pylint: disable=line-too-long
    request_url = f'https://tidesandcurrents.noaa.gov/api/datagetter?product=predictions&begin_date={current_day_formatted}+00:00&end_date={tomorrow_formatted}+23:59&datum=MLLW&station={station_id}&time_zone=LST_LDT&units=english&format=json&application=NOS.COOPS.TAC.STATIONHOME'

    response: requests.Response = requests.get(request_url)
    return response.text

def tides_by_station(station_id: int, date: datetime = datetime.now()):
    """Retrieve the tides for a particular NOAA station"""

    tide_points = parse_tide_points(_make_noaa_request(station_id=station_id, date=date))

    return parse_boundaries(tide_points)
