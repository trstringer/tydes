"""Test module for day_tide module testing"""

from .day_tide import parse_boundaries
from .test_global import RAW_DATA_FILENAME
from .tide_point import parse_tide_points

def test_parse_boundaries() -> None:
    """Test parse_boundaries"""

    with open(RAW_DATA_FILENAME) as raw_data:
        tide_points = parse_tide_points(''.join(raw_data.readlines()))

    tides_of_the_day = parse_boundaries(tide_points)

    assert tides_of_the_day

    print(f'How many tide boundaries? {len(tides_of_the_day.boundaries)}')
    for idx, boundary in enumerate(tides_of_the_day.boundaries):
        # pylint: disable=line-too-long
        print(f'Boundary {idx}: type={boundary.boundary_type} level={boundary.point.level} occur_on={boundary.point.occur_on}')
