"""Global test usage components"""

import os

RAW_DATA_FILENAME = os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    'test_tide_raw_data.txt')
