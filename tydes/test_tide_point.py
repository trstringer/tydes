"""Test module for tide_point module"""

from .test_global import RAW_DATA_FILENAME
from .tide_point import parse_tide_points

def test_parse_tide_points() -> None:
    """Test parse_tide_points"""

    with open(RAW_DATA_FILENAME) as raw_data:
        tide_points = parse_tide_points(''.join(raw_data.readlines()))

    assert len(tide_points) > 1
