"""TideBoundary class definition"""

from .tide_boundary_type import TideBoundaryType
from .tide_point import TidePoint

# pylint: disable=too-few-public-methods
class TideBoundary:
    """Contains the definition for a tide boundary either high or low tide"""

    def __init__(self, boundary_type: TideBoundaryType, point: TidePoint):
        """Initialize the object"""

        self.boundary_type = boundary_type
        self.point = point
