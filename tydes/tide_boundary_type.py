"""TideBoundaryType enum"""

from enum import Enum

class TideBoundaryType(Enum):
    """Defines the enum for a tide boundary type"""

    LOW = 1
    HIGH = 2
