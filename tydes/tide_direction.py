"""TideDirection enum definition"""

from enum import Enum

class TideDirection(Enum):
    """Enum to explain the tide direction"""

    RISING = 1
    FALLING = 2
