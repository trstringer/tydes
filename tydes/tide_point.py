"""TidePoint class definition"""

from datetime import datetime
import json
from typing import List

# pylint: disable=too-few-public-methods
class TidePoint:
    """Contains the definition for a single tide point"""

    def __init__(self, level: float, occur_on: datetime):
        """Initialize the tide point"""

        self.level = level
        self.occur_on = occur_on

def parse_tide_points(raw: str) -> List[TidePoint]:
    """Parse tide point objects from a raw NOAA string"""

    # raw data format:
    #
    # "t" - the tide point date/time
    # "v" - the tide point water level
    #
    # {
    #   "predictions": [
    #     {
    #       "t": "2018-07-13 12:00",
    #       "v": "9.746"
    #     },
    #     {
    #       "t": "2018-07-13 12:06",
    #       "v": "9.745"
    #     }
    #   ]
    # }

    raw_json = json.loads(raw)
    return [
        TidePoint(
            level=float(point['v']),
            occur_on=point['t'])
        for point
        in raw_json['predictions']]
